package Characters;

import Elements.PrimaryAttribute;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    //Expects a characters level is 1 after creation.
    @Test
    void getLevelOnCreation() {
        Mage mage = new Mage("Lene");
        int actual = mage.getLevel();
        int expected = 1;
        assertEquals(expected, actual);
    }

    //Expects a characters level is 2 after levelling up once.
    @Test
    void getLevelOnLevelUp(){
        Ranger ranger = new Ranger("Ola");
        ranger.levelUp();
        int actual = ranger.getLevel();
        int expected = 2;
        assertEquals(expected, actual);
    }

    //Tests that the Character classes contain the correct values on creation
    @Test
    void getPrimaryAMage() {
        Mage mage = new Mage("Tone");
        PrimaryAttribute primaryA = mage.getPrimaryA();
        int[] expected = new int[]{5, 1, 1, 8};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryARanger() {
        Ranger ranger = new Ranger("Karl");
        PrimaryAttribute primaryA = ranger.getPrimaryA();
        int[] expected = new int[]{8, 1, 7, 1};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryARouge() {
        Rouge rouge = new Rouge("Trine");
        PrimaryAttribute primaryA = rouge.getPrimaryA();
        int[] expected = new int[]{8, 2, 6, 1};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryAWarrior() {
        Warrior warrior = new Warrior("Ole");
        PrimaryAttribute primaryA = warrior.getPrimaryA();
        int[] expected = new int[]{10, 5, 2, 1};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    //Tests that the Character classes contain the correct values after levelling up once.
    @Test
    void getPrimaryAOnLevelUpMage() {
        Mage mage = new Mage("Trine");
        mage.levelUp();
        PrimaryAttribute primaryA = mage.getPrimaryA();
        int[] expected = new int[]{8, 2, 2, 13};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryAOnLevelUpRanger() {
        Ranger ranger = new Ranger("Trine");
        ranger.levelUp();
        PrimaryAttribute primaryA = ranger.getPrimaryA();
        int[] expected = new int[]{10, 2, 12, 2};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryAOnLevelUpRouge() {
        Rouge rouge = new Rouge("Trine");
        rouge.levelUp();
        PrimaryAttribute primaryA = rouge.getPrimaryA();
        int[] expected = new int[]{11, 3, 10, 2};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void getPrimaryAOnLevelUpWarrior() {
        Warrior warrior = new Warrior("Trine");
        warrior.levelUp();
        PrimaryAttribute primaryA = warrior.getPrimaryA();
        int[] expected = new int[]{15, 8, 4, 2};
        int[] actual = new int[]{primaryA.getVitality(), primaryA.getStrength(), primaryA.getDexterity(), primaryA.getIntelligence()};
        assertArrayEquals(expected, actual);
    }
}