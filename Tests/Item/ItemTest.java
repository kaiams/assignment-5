package Item;

import Characters.Warrior;
import Elements.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    //Expects error thrown when trying to equip weapon with a higher level requirement.
    @Test
    void equipItemWeaponWithHigherLevel(){
        Warrior warrior = new Warrior("Anders");
        Weapon axe = new Weapon("Common Axe", 2, 7, 1.1, WeaponType.AXE);
        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equipItem(axe, Slot.WEAPON));

        String expected = "Invalid: The weapon requires a higher level";
        String actual =exception.getMessage();
        assertEquals(expected, actual);
    }

    //Expects error thrown when trying to equip armor with a higher level requirement
    @Test
    void equipItemArmorWithHigherLevel(){
        Warrior warrior = new Warrior("Anders");
        Armor plateBody = new Armor("Common Plate Body Armor", 2, ArmorType.PLATE,Slot.BODY, new PrimaryAttribute(2, 3, 4, 7));
        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equipItem(plateBody, plateBody.getSlot()));

        String expected = "Invalid: The armor requires a higher level";
        String actual =exception.getMessage();
        assertEquals(expected, actual);
    }

    //Expects error thrown when trying to equip weapon that cannot be used by the character class.
    @Test
    void equipItemWrongWeapon(){
        Warrior warrior = new Warrior("Anders");
        Weapon bow = new Weapon("Common Bow", 1, 3, 1.9, WeaponType.BOW);

        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equipItem(bow, Slot.WEAPON));

        String expected = "Invalid: The character cannot equip this weapon";
        String actual =exception.getMessage();
        assertEquals(expected, actual);
    }

    //Expects error thrown when trying to equip armor that cannot be used by the character class.
    @Test
    void equipItemWrongArmor(){
        Warrior warrior = new Warrior("Anders");
        Armor cloth = new Armor("Common Cloth Head Armor", 1, ArmorType.CLOTH, Slot.HEAD, new PrimaryAttribute(0, 0, 5, 1));

        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equipItem(cloth, cloth.getSlot()));

        String expected = "Invalid: The character cannot equip this armor";
        String actual =exception.getMessage();
        assertEquals(expected, actual);
    }

    //Expects true when equipping valid weapon.
    @Test
    void equipItemValidWeapon(){
        Warrior warrior = new Warrior("Anders");
        Weapon axe = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        boolean actual = false;

        try{
            actual =  warrior.equipItem(axe, Slot.WEAPON);
        }
       catch (Exception e) {
            e.printStackTrace();
       }
        assertTrue(actual);
    }

    //Expects true when equipping valid armor.
    @Test
    void equipItemValidArmor(){
        Warrior warrior = new Warrior("Anders");
        Armor plateBody = new Armor("Common Plate Body Armor", 1, ArmorType.PLATE,Slot.BODY, new PrimaryAttribute(2, 3, 4, 7));
        boolean actual = false;

        try{
            actual =  warrior.equipItem(plateBody, plateBody.getSlot());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(actual);
    }

    //Expects base DPS when no weapon or armor is equipped.
    @Test
    void getDPSBase(){
        Warrior warrior = new Warrior("Anders");
        double expected = 1*(1 +((float)5/100));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);
    }

    //Expects base DPS when only weapon equipped.
    @Test
    void getDPSWithWeapon(){
        Warrior warrior = new Warrior("Anders");
        Weapon axe = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        try{
            warrior.equipItem(axe, Slot.WEAPON);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        double expected = (7 * 1.1)*(1 +((float)5/100));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);
    }

    //Expects base DPS when weapon and armor equipped.
    @Test
    void getDPSWithWeaponAndArmor(){
        Warrior warrior = new Warrior("Anders");
        Weapon axe = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        Armor plateBody = new Armor("Common Plate Body Armor", 1, ArmorType.PLATE,Slot.BODY, new PrimaryAttribute(2, 3, 4, 7));

        try{
            warrior.equipItem(axe, Slot.WEAPON);
            warrior.equipItem(plateBody, plateBody.getSlot());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        double expected = (7 * 1.1)*(1 +((float)(5+1)/100));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);
    }
}