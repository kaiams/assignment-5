package Elements;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }

    //Attribute getters
    public int getStrength(){
        return strength;
    }

    public int getDexterity(){
        return dexterity;
    }

    public int getIntelligence(){
        return intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    //Attribute increase
    public void strengthIncrease(int i){
        strength += i;
    }
    public void dexterityIncrease(int i){
        dexterity += i;
    }
    public void intelligenceIncrease(int i){
        intelligence += i;
    }
    public void vitalityIncrease(int i){
        vitality += i;
    }
}
