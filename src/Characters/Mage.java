package Characters;

import Elements.Slot;

public class Mage extends Character{

    public Mage(String name){
        //initialize values.
        super(name, 1, 1, 8, 5);
        allowedWeapons.add("STAFF");
        allowedWeapons.add("WAND");
        allowedArmors.add("CLOTH");
    }


    @Override
    public void levelUp() {
        super.levelUp();
        primaryA.strengthIncrease(1);
        primaryA.dexterityIncrease(1);
        primaryA.intelligenceIncrease(5);
        primaryA.vitalityIncrease(3);
    }

    @Override
    public double getDPS() {
        double weaponDPS = 1;
        if(equipped.get(Slot.WEAPON) != null){
            weaponDPS = equipped.get(Slot.WEAPON).getDPS();
        }
        return weaponDPS * (1 + ((float)primaryA.getIntelligence()/100));
    }
}
