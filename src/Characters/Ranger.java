package Characters;

import Elements.Slot;

public class Ranger extends Character{

    public Ranger(String name){
        //initialize values.
        super(name, 1, 7, 1, 8);
        allowedWeapons.add("BOW");
        allowedArmors.add("LEATHER");
        allowedArmors.add("MAIL");
    }

    @Override
    public void levelUp(){
        super.levelUp();
        primaryA.strengthIncrease(1);
        primaryA.dexterityIncrease(5);
        primaryA.intelligenceIncrease(1);
        primaryA.vitalityIncrease(2);
    }

    @Override
    public double getDPS() {
        double weaponDPS = 1;
        if(equipped.get(Slot.WEAPON) != null){
            weaponDPS = equipped.get(Slot.WEAPON).getDPS();
        }
        return weaponDPS * (1 + ((float)primaryA.getDexterity()/100));

    }
}
