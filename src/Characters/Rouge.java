package Characters;

import Elements.Slot;

public class Rouge extends Character{
    public Rouge(String name){
        //initialize values.
        super(name, 2, 6, 1, 8);

        allowedWeapons.add("DAGGER");
        allowedWeapons.add("SWORD");
        allowedArmors.add("LEATHER");
        allowedArmors.add("MAIL");
    }

    @Override
    public void levelUp(){
        super.levelUp();
        primaryA.strengthIncrease(1);
        primaryA.dexterityIncrease(4);
        primaryA.intelligenceIncrease(1);
        primaryA.vitalityIncrease(3);
    }

    @Override
    public double getDPS() {
        double weaponDPS = 1;
        if(equipped.get(Slot.WEAPON) != null){
            weaponDPS = equipped.get(Slot.WEAPON).getDPS();
        }
        return weaponDPS * (1 + ((float)primaryA.getDexterity()/100));
    }
}
