package Characters;
import Elements.InvalidArmorException;
import Elements.InvalidWeaponException;
import Elements.PrimaryAttribute;
import Item.Item;
import Elements.Slot;

import java.util.ArrayList;
import java.util.HashMap;

abstract class Character {
    protected String name;
    protected int level = 1;
    protected PrimaryAttribute primaryA;
    protected HashMap<Slot, Item> equipped;
    protected ArrayList<String> allowedWeapons;
    protected ArrayList<String> allowedArmors;



    public Character(String name, int strength, int dexterity, int intelligence, int vitality){
        this.name = name;
        primaryA = new PrimaryAttribute(strength, dexterity, intelligence, vitality);
        //Sets all slots to null as the Character starts with nothing equipped.
        equipped = new HashMap<>();
        equipped.put(Slot.HEAD, null);
        equipped.put(Slot.BODY, null);
        equipped.put(Slot.LEGS, null);
        equipped.put(Slot.WEAPON, null);

        allowedWeapons = new ArrayList<>();
        allowedArmors = new ArrayList<>();
    }

    public int getLevel(){
        return level;
    }

    public PrimaryAttribute getPrimaryA(){
        return primaryA;
    }

    public int getStrength(){
        int strength = primaryA.getStrength();
        for (Slot key : equipped.keySet()){
            if(key.toString().equals("Weapon") && equipped.get(key)!= null) {
                strength += equipped.get(key).getPrimaryA().getStrength();
            }
        }
        return strength;
    }

    public int getDexterity(){
        int dexterity = primaryA.getDexterity();
        for (Slot key : equipped.keySet()){
            if(key.toString().equals("Weapon") && equipped.get(key)!= null) {
                dexterity += equipped.get(key).getPrimaryA().getDexterity();
            }
        }
        return dexterity;
    }

    public int getIntelligence(){
        int intelligence = primaryA.getIntelligence();
        for (Slot key : equipped.keySet()){
            if(key.toString().equals("Weapon") && equipped.get(key)!= null) {
                intelligence += equipped.get(key).getPrimaryA().getIntelligence();
            }
        }
        return intelligence;
    }

    public int getVitality(){
        int vitality = primaryA.getStrength();
        for (Slot key : equipped.keySet()){
            if(key.toString().equals("Weapon") && equipped.get(key)!= null) {
                vitality += equipped.get(key).getPrimaryA().getVitality();
            }
        }
        return vitality;
    }

    public String printAllAttributes(){
        int strength = getStrength();
        int dexterity = getDexterity();
        int intelligence = getIntelligence();
        int vitality = getVitality();

        double totalDPS = getDPS();

        String allStats = "Stats for character: " + name + "\nLevel: " + level;
        allStats += "\nStrength: " + strength + "\nDexterity: " + dexterity + "\nIntelligence: " + intelligence;
        allStats += "\nVitality: " + vitality + "\nDPS: " + totalDPS;
        return allStats;
    }

    //for equipping item. Throws exception if the item is invalid or the characters level is too low.
    public boolean equipItem(Item item, Slot slot) throws InvalidWeaponException, InvalidArmorException {
        String[] itemTypes = item.getType();
        String itemClass = itemTypes[0];
        String type = itemTypes[1];

        //throws correct error by checking for the Item class first.
        if(itemClass.equals("Weapon")){
            if(item.getReqLevel() > level){
                throw new InvalidWeaponException("Invalid: The weapon requires a higher level");
            }
            else if(!allowedWeapons.contains(type)) {
                throw new InvalidWeaponException("Invalid: The character cannot equip this weapon");
            }
        }
        else{
            if(item.getReqLevel() > level){
                throw new InvalidArmorException("Invalid: The armor requires a higher level");
            }
            else if(!allowedArmors.contains(type)){
                throw new InvalidArmorException("Invalid: The character cannot equip this armor");
            }
        }
        //if no error is thrown, equips by replacing item in the correct slot.
        equipped.replace(slot, item);
        return true;
    }

    //method for levelling up.
    public  void levelUp(){
        level++;
    }

    public abstract double getDPS();

}
