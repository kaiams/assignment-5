package Characters;

import Elements.Slot;

public class Warrior extends Character{
    public Warrior(String name){
        //initialize values.
        super(name, 5, 2, 1, 10);
        allowedWeapons.add("AXE");
        allowedWeapons.add("HAMMER");
        allowedWeapons.add("SWORD");
        allowedArmors.add("MAIL");
        allowedArmors.add("PLATE");
    }

    @Override
    public void levelUp(){
        super.levelUp();
        primaryA.strengthIncrease(3);
        primaryA.dexterityIncrease(2);
        primaryA.intelligenceIncrease(1);
        primaryA.vitalityIncrease(5);
    }

    @Override
    public double getDPS() {
        double weaponDPS = 1;
        if(equipped.get(Slot.WEAPON) != null){
            weaponDPS = equipped.get(Slot.WEAPON).getDPS();
        }
        return weaponDPS * (1 + ((float)primaryA.getStrength()/100));
    }
}
