package Item;

import Elements.PrimaryAttribute;
import Elements.Slot;
import Elements.WeaponType;

public class Weapon extends Item {

    protected int damage;
    protected double attackSpeed;
    protected double DPS;
    protected WeaponType type;


    public Weapon(String name, int reqLevel, int damage, double attackSpeed, WeaponType type){
        super(name, reqLevel);

        slot = Slot.WEAPON;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        DPS = damage * attackSpeed;
        this.type = type;
    }

    @Override
    public String[] getType() {
        return new String[]{"Weapon", type.toString()};
    }

    @Override
    public PrimaryAttribute getPrimaryA(){
        return null;
    }

    @Override
    public double getDPS() {
        return DPS;
    }
}
