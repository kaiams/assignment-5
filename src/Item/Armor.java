package Item;

import Elements.ArmorType;
import Elements.PrimaryAttribute;
import Elements.Slot;

public class Armor extends Item{
    private ArmorType type;
    private PrimaryAttribute primaryA;

    public Armor(String name, int reqLevel, ArmorType type, Slot slot, PrimaryAttribute primaryA) {
        super(name, reqLevel);
        this.slot = slot;
        this.type = type;
        this.primaryA = primaryA;
    }

    @Override
    public String[] getType() {
        return new String[]{"Armor", type.toString()};
    }

    @Override
    public PrimaryAttribute getPrimaryA(){
        return primaryA;
    }

    @Override
    public double getDPS() {
        return 0;
    }
}
