package Item;
import Elements.PrimaryAttribute;
import Elements.Slot;

public abstract class Item {
    protected String name;
    protected int reqLevel;
    protected Slot slot;

    public Item(String name, int reqLevel){
        this.name = name;
        this.reqLevel = reqLevel;
    }

    public String getName(){
        return name;
    }

    public int getReqLevel(){
        return reqLevel;
    }

    public Slot getSlot(){
        return slot;
    }

    //returns array containing type of item "Weapon" or "Armor" and the detailed category;
    public abstract String[] getType();
    public abstract PrimaryAttribute getPrimaryA();
    public abstract double getDPS();
}
