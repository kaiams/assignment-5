# Assignment 5

Console application containing characters and items the characters can equip.

## Content
#### Characters:
- Mage 
- Ranger
- Rouge
- Warrior

The character decides what stats one gets and items one can equip
#### Items: 
- Armor 
- Weapon

Armors enhance the primary attributes and weapons the DPS

## Testing
Tests are included in the Tests folder 